FROM python:3.8.0-slim-buster

LABEL maintainer "Ian Wang <jingwangian@gmail.com>"

USER root
# RUN apt-get update 

RUN apt-get update && apt-get -y --no-install-recommends install \
    ca-certificates \
    curl \
    gcc \
    g++ \
    unixodbc unixodbc-dev \
    vim \
    gpg

RUN apt-get install -y gosu

# && apt-get install -y build-essential

WORKDIR /tmp

ENV FLASK_APP=run.py
ENV FLASK_ENV=development

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

EXPOSE 5500

WORKDIR /opt/server

COPY requirements.txt /tmp/requirements.txt
RUN pip3 --no-cache install -r /tmp/requirements.txt

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
